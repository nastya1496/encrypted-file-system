// dialog.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
using namespace std;

const char *msgs[] = { "0. Quit", "1. Edit user table", "2. Add file/directory", "3. Open file",
"4. Moving file","5. Copy file", "6. Delete file", "7. Show information about file", "8. Rename", "9. Edit access table" ,
"10. Statistic", "11. Encrypt/ file","12. End work" }; // ������ �����������
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]); // ���������� �����������
int pr = 0;
const char *errmsgs[] = { "Tablitsa zapolnena", "Ok" };
const exception check_parent("You want choose parent.");

int dialog(const char *msgs[], int);
int D_BeginWork(filesystem &),
D_EditUserTable(filesystem &),
D_AddFile(filesystem &),
D_OpenFile(filesystem &),
D_MovingFile(filesystem &v),
D_CopyFile(filesystem &),
D_DeleteFile(filesystem &),
D_ShowInformation(filesystem &),
D_Rename(filesystem &),
D_EditAccessTable(filesystem &),
D_Statistic(filesystem &),
D_EncryptFile(filesystem &),
D_EndWork(filesystem &);

void PrintTable(std::vector<user> m);
void AddAccess(directory &s, access n);
vector<access> CreateAccessList(filesystem &s);
void ShowDirectory(directory &g, const user &u);
OnOff CheckName(directory &d, string &s, const user &n);
void PrintAccessTable(std::vector<access> m);
set<structelem>::iterator ChooseFile(filesystem &s);


int(*fptr[])(filesystem &) = { NULL, D_EditUserTable, D_AddFile, D_OpenFile, D_MovingFile, D_CopyFile
			, D_DeleteFile, D_ShowInformation, D_Rename, D_EditAccessTable, D_Statistic, D_EncryptFile, D_EndWork};

int getnum(int &);

int getnum(int &a){

	std::cin >> a;					//���� ������ �����			
	if (std::cin.eof())
		return 0;
	if (!std::cin.good()){			//����� ������
		return -1;
	}
	return 1;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		puts(errmsg);
		errmsg = "You are wrong. Repeat, please!";
		// ����� ������ ����������� 
		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		puts("Make your choice: --> ");
		n = getnum(rc); // ���� ������ ������������ 
		//ignore();
		std::cin.clear();
		_flushall();
		if (n == 0) // ����� ����� - ����� ������ 
			rc = 0;
	} while (rc < 0 || rc > N);
	return rc;
}


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		filesystem t;
		D_BeginWork(t);
		int rc;
		while (rc = dialog(msgs, NMsgs))
			if (!fptr[rc](t))
				break;
		printf("That's all. Bye!\n");
	}
	catch (exception &a) {
		std::cout << a.what() << std::endl;
		return 1;
	}
	return 0;
}

int D_BeginWork(filesystem &s){
	std::vector<user> t = s.getTable();
	std::cout << "Choose user:" << std::endl;
	for (size_t i = 0; i < t.size(); i++)
	{
		std::cout << (i+1)<<". "<< t[i].name << "\t" << t[i].ID << std::endl;
	}
	int c;
	if (getnum(c) < 1){
		return 0;
	}
	c--;
	size_t uh = c;
	if (uh <= t.size()-1){
		user u = { t[c].ID };
		s.BeginWork(u);
	}
	else{
		std::cout << "User with this number does  not available" << std::endl;
	}
	return 1;
}

int D_EditUserTable(filesystem &s){
	try{
		std::vector<user> &f = s.EditTable();
		PrintTable(f);
		std::cout << "What do you want to do?" << std::endl;
		const char *m[] = { "1. Add user", "2. Delete user", "3. Modify user's name" };
		int k = dialog(m, 3);
		if (k == 1){
			std::cout << "Enter name" << std::endl;
			char n[200];
			std::cin.getline(n, 199);
			string nm(n);
			int key = 1 + rand() % 21;
			int ID = s.GetLastID() + 1;
			s.SetLastID(ID);
			user u = { ID, nm, key };
			f.push_back(u);
			std::cout << std::endl;
			PrintTable(f);
			access h = { { ID, nm, key }, True, False, True };
			AddAccess(s.SetRoot(), h);		
			return 1;
		}
		if (k == 2){
			std::cout << "Enter user's number" << std::endl;
			int c;
			if (getnum(c) < 1){
				return 0;
			}
			size_t z = c;
			if ((z <= f.size()) && (c>1)){
				std::vector<user>::iterator pp = f.begin();
				for (int i = 0; i < (c - 1); i++, pp++);
				f.erase(pp);
			}
			else{
				std::cout << "User with this number does  not available" << std::endl;
			}
			std::cout << std::endl;
			PrintTable(f);
			return 1;
		}
		if (k == 3){
			std::cout << "Enter user's number" << std::endl;
			int c;
			if (getnum(c) < 1){
				return 0;
			}
			size_t z = c;
			if ((z <= f.size())&&(c>1)){
				std::cout << "Enter name" << std::endl;
				char m[200];
				_flushall();
				std::cin.getline(m, 199);
				string nm(m);
				std::vector<user>::iterator pp = f.begin();
				pp++;
				for (int i = 1; i < (c - 1); i++, pp++);
				pp->name.clear();
				pp->name = nm;
			}
			else{
				std::cout << "User with this number does not available" << std::endl;
			}
			std::cout << std::endl;
			PrintTable(f);
			return 1;
		}
		
	}
	catch (std::exception &a){
		std::cout << a.what() << std::endl;
		return 1;
	}
	
	return 1;
}

int D_AddFile(filesystem &s){
	std::cout << "What you want add? File = 1, Directory = 2" << std::endl;
	int c;
	if (getnum(c) < 1){
		return 0;
	}
	if ((c == 1) || (c == 2)){
		if (c == 1){
			vector<descriptor> w;
			descriptor p = { "MAIN", 0, "" };
			w.push_back(p);
			vector<access> x = CreateAccessList(s);
			string xz;
			OnOff a = False;
			while (!a){
				std::cout << "Enter name of file" << std::endl;
				_flushall();
				char j[200];
				cin.getline(j, 199);
				xz=j;
				a = CheckName(*s.GetNowDirectory(), xz, s.GetNowUser());
				if (a == False){
					cout << "This name has already exist.";
				}
			}
			simplefile *f = new simplefile(x, xz, s.GetNowDirectory(), s.GetNowUser(), 1, w);
			structelem q = { f->getname(), f };
			s.GetNowDirectory()->setstructtable().insert(q);
			PrintAccessTable(f->getaccess());
		}
		if (c == 2){
			vector<access> x = CreateAccessList(s);
			string xz;
			OnOff a = False;
			while (!a){
				std::cout << "Enter name of file" << std::endl;
				_flushall();
				char j[200];
				cin.getline(j, 199);
				xz = j;
				a = CheckName(*s.GetNowDirectory(), xz, s.GetNowUser());
				if (a == False){
					cout << "This name has already exist.";
				}
			}
			directory *f = new directory(x, xz, s.GetNowDirectory(),0, 0);
			structelem q = { f->getname(), f };
			s.GetNowDirectory()->setstructtable().insert(q);
		}
		
	}
	else{
		std::cout << "You are wrong." << std::endl;
	}
	return 1;
}

int D_OpenFile(filesystem &s){

	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		s.SetNowDirectory(s.GetNowDirectory()->getparent());
		ShowDirectory(*(s.GetNowDirectory()), s.GetNowUser());
		return 1;
	}

	if ((*pp).descriptoroffile->iAm() != "Directory"){
		
		
		int f = 0;
		cout << "You want read(0) or write(1) or Edit(2)" << endl;
		int c;
		if (getnum(c) < 1){
			return 0;
		}
		if ((c != 0) && (c != 1) && (c != 2)){
			cout << "You are wrong." << endl;
			return 1;
		}
		OpenClose v;
		if (c == 0){ v = OpenRead; }
		if (c == 1){
			v = OpenWrite;
			
			if ((*pp).descriptoroffile->iAm() == "Encrypted File"){
				f = 1;
				directory *par = (*pp).descriptoroffile->getparent();
				string name = (*pp).nameoffile;
				s.EncryptionFile(*dynamic_cast<encryptedfile *>((*pp).descriptoroffile));
				pp = par->setstructtable().begin();
				for (; (*pp).nameoffile != name; pp++);
			}
		}
		if (c == 2){ 
			v = OpenEdit;
			if ((*pp).descriptoroffile->iAm() == "Encrypted File"){
			f = 1;
			directory *par = (*pp).descriptoroffile->getparent();
			string name = (*pp).nameoffile;
			s.EncryptionFile(*dynamic_cast<encryptedfile *>((*pp).descriptoroffile));
			pp = par->setstructtable().begin();
			for (; (*pp).nameoffile != name; pp++);
		}
		}
		try {
			simplefile* dsa = dynamic_cast<simplefile*>((*pp).descriptoroffile);
			dsa->ConditionFile(v, s.GetNowUser());
			if (c == 0){
				cout << dsa->ShowInformation() << endl;
			}


			if (c == 1){

				cout << "Enter new information of file" << endl;
				_flushall();
				char j[200];
				cin.getline(j, 199);
				const string xz(j);
				dsa->setstream()[0].data = xz;
			}

			if (c == 2){
				cout << dsa->getstream()[0].data << endl;
				cout << "Enter new information of file" << endl;
				_flushall();
				char j[200];
				cin.getline(j, 199);
				const string xz(j);
				dsa->setstream()[0].data = xz;
			}

			v = Close;
			dsa->ConditionFile(v, s.GetNowUser());
			if (f)
				s.EncryptionFile(*dsa);
		}
		catch (exception &a){
			cout << a.what() << endl;
		}
	}
	else{
		directory *jkl = dynamic_cast<directory*>((*pp).descriptoroffile);
		s.SetNowDirectory(jkl);
		ShowDirectory(*jkl, s.GetNowUser());
	}
	return 1;
}

int D_MovingFile(filesystem &s){
	set<structelem>::iterator ppr;
	try {
		ppr = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	int q = 1;
	while (q != 0){
		cout << "Choose directory, please." << endl;
		_flushall();
		char j[200];
		cin.getline(j, 199);
		string xzc(j);
		if (s.GetNowDirectory()->getparent() && xzc == "..."){
			s.SetNowDirectory(s.GetNowDirectory()->getparent());
			ShowDirectory(*(s.GetNowDirectory()), s.GetNowUser());

		}
		else{

			set<structelem>::iterator pp = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
			for (; pp != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp++){
				if (xzc == (*pp).nameoffile){
					break;
				}
			}
			if ((*pp).descriptoroffile->iAm() != "Directory"){
				cout << "Choose directory, please." << endl;
			}
			else{
				directory *jkl = dynamic_cast<directory*>((*pp).descriptoroffile);
				s.SetNowDirectory(jkl);
				ShowDirectory(*jkl, s.GetNowUser());
			}
		}
		cout << "Insert in this directory? Yes=0, No=1" << endl;
		if (getnum(q) < 1){
			return 0;
		}
		if ((q != 0) && (q != 1)){
			cout << "You are wrong." << endl;
			return 1;
		}

	}
	if (CheckName(*s.GetNowDirectory(), (*ppr).nameoffile, s.GetNowUser())){

		(*ppr).descriptoroffile->MovingFile(s.GetNowUser(), *s.GetNowDirectory());
	}
	else{
		(*ppr).descriptoroffile->setname((*ppr).descriptoroffile->getname() + "-1");
	}
	return 1;
}

int D_CopyFile(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	int q ;
	cout << "Copy in this directory? Yes=0, No=1" << endl;
	if (getnum(q) < 1){
		return 0;
	}
	if ((q != 0) && (q != 1)){
		cout << "You are wrong." << endl;
		return 1;
	}
	if (q == 0){ (*pp).descriptoroffile->CopyFile(s.GetNowUser(), *s.GetNowDirectory()); }

	while (q != 0){
		cout << "Choose directory in which you want copy this file, please." << endl;
		_flushall();
		char j[200];
		cin.getline(j, 199);
		string xzc(j);
		if (s.GetNowDirectory()->getparent() && xzc == "..."){
			(*pp).descriptoroffile->CopyFile(s.GetNowUser(), *s.GetNowDirectory()->getparent());
		}
		else{

			set<structelem>::iterator pp = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
			for (; pp != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp++){
				if (xzc == (*pp).nameoffile){
					break;
				}
			}
			if ((*pp).descriptoroffile->iAm() != "Directory"){
				cout << "Choose directory, please." << endl;
			}
			else{
				directory *jkl = dynamic_cast<directory*>((*pp).descriptoroffile);
				s.SetNowDirectory(jkl);
				ShowDirectory(*jkl, s.GetNowUser());
			}
		}
		cout << "Copy in this directory? Yes=0, No=1" << endl;
		if (getnum(q) < 1){
			return 0;
		}
		if ((q != 0) && (q != 1)){
			cout << "You are wrong." << endl;
			return 1;
		}
	}
	if (CheckName(*s.GetNowDirectory(), (*pp).nameoffile, s.GetNowUser())){

		(*pp).descriptoroffile->CopyFile(s.GetNowUser(), *s.GetNowDirectory());
	}
	else{
		(*pp).descriptoroffile->setname((*pp).descriptoroffile->getname() + "-1");
	}

	return 1;
}

int D_DeleteFile(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	(*pp).nameoffile.clear();
	(*pp).descriptoroffile->DeleteFile(s.GetNowUser());
	return 1;
}

int D_ShowInformation(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	cout<<(*pp).nameoffile<< endl;
	(*pp).descriptoroffile->Print(cout);
	cout << endl;
	return 1;
}

int D_Rename(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	cout << "Enter new name" << endl;
	_flushall();
	char o[200];
	cin.getline(o, 199);
	string xz;
	xz=o;
	if(CheckName(*s.GetNowDirectory(), xz, s.GetNowUser())){
		(*pp).descriptoroffile->RenameFile(s.GetNowUser(), xz);
		(*pp).nameoffile.clear();
		(*pp).nameoffile = xz;
	}
	return 1;
}

int D_EditAccessTable(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	vector<access> x = CreateAccessList(s);
	(*pp).descriptoroffile->setaccess().clear();
	(*pp).descriptoroffile->setaccess() = x;
	
	PrintAccessTable(x);
	return 1;
}

int D_Statistic(filesystem &s){
	cout << "Statistic" << endl;
	statistic a = {0, 0, 0, 0};
	a=s.Statistic(a,& s.SetRoot());
	cout << setw(10)<<"Directory" << std::setw(15) << "Simple file" << std::setw(15) << "Encrypted file" 
		<< std::setw(15) << "All size" << endl;
	cout << setw(10)<<a.directorynumber << std::setw(15) << a.simplefilenumber << std::setw(15)
		<< a.encryptedfilenumber << std::setw(9) << a.allsize <<setw(6)<<" bytes"<< endl;
	return 1;
}

int D_EncryptFile(filesystem &s){
	set<structelem>::iterator pp;
	try {
		pp = ChooseFile(s);
	}
	catch (const exception){
		cout << "You cannot choose this directory." << endl;
		return 1;
	}
	if ((*pp).descriptoroffile->iAm() == "Directory"){
		cout << "You are wrong. Choose FILE.";
		return 1;
	}
	if ((*pp).descriptoroffile->iAm() == "Simple File"){
		simplefile *y = dynamic_cast<simplefile*>((*pp).descriptoroffile);
		s.EncryptionFile(*y);
		return 1;
	}
	if ((*pp).descriptoroffile->iAm() == "Encrypted File"){
		encryptedfile *y = dynamic_cast<encryptedfile*>((*pp).descriptoroffile);
		s.EncryptionFile(*y);
		return 1;
	}
	return 1;
}

int D_EndWork(filesystem &s){
	s.Setnowuser({ 0 });
	return D_BeginWork(s);
}




void ShowDirectory(directory &g,const user &u){
	if (g.getparent()){
		cout << "..." << endl;
	}

	set<structelem>::iterator pp = g.ShowStructure(u).begin();
	for (; pp != g.ShowStructure(u).end(); pp++){
		cout << (*pp) << endl;
	}
}

void PrintTable(std::vector<user> m){
	std::cout.width(10);
	std::cout << "Name" << std::setw(10) << "ID" << std::setw(5)<< "Key" << std::endl;
	for (size_t i=0; i < m.size(); i++){
		std::cout << (i + 1) << ". " << std::setw(7) << m[i].name << std::setw(10) << m[i].ID << std::setw(5) << m[i].key << std::endl;
	}
}

vector<access> CreateAccessList(filesystem &s){
	PrintTable(s.getTable());
	cout << endl;
	
	int c=1;
	
	vector<access> as;
	access a = { { 1000, "Admin", 1 }, True, True, True };
	as.push_back(a);
	if (s.GetNowUser().ID != 1000){
		a = { { s.GetNowUser().ID, s.GetNowUser().name, s.GetNowUser().key }, True, True, True };
		as.push_back(a);
	}

	while (c){
		cout << "Enter user's number which you want to add. (Stop enter= 0)" << endl;
		if (getnum(c) < 1){
			throw exception("You are wrong");
		}
		if ((!c)||(c==1)){ break; }

		vector<access>::iterator pp = as.begin();
		for (; pp != as.end(); pp++){
			if (s.getTable()[c-1].ID == (*pp).UserName.ID){
				break;
			}
		}
		if ((pp!=as.end()) && s.getTable()[c - 1].ID == (*pp).UserName.ID){
			cout << "This user is already in the list"<<endl;
			continue;
		}
		size_t z = c;
		if (z <= s.getTable().size()){
			int l;
			OnOff d;
			cout << "This user can read?(0/1)" << endl;
			if (getnum(l) < 1){
				throw exception("You are wrong");
			}
			d = l ? True : False;
			a.read = d;
			cout << "This user can write?(0/1)" << endl;
			if (getnum(l) < 1){
				throw exception("You are wrong");
			}
			d = l ? True : False;
			a.write = d;
			cout << "This user can execute?(0/1)" << endl;
			if (getnum(l) < 1){
				throw exception("You are wrong");
			}
			d = l ? True : False;
			a.execute = d;
			a.UserName.key = s.getTable()[c - 1].key;
			a.UserName = {s.getTable()[c - 1].ID, s.getTable()[c-1].name, s.getTable()[c-1].key};
			as.push_back(a);
		}

	}
	return as;
}

void AddAccess(directory &s, access n){
	vector<access> r;
	for (int i = 0; i < s.getaccesssize(); i++){
		r.push_back(s.getaccess()[i]);
	}
	r.push_back(n);
	s.setaccess().clear();
	vector<access> & t = s.setaccess();
	t= r;

}

OnOff CheckName(directory &d, string &s, const user &n){
	set<structelem>::iterator pp = d.ShowStructure(n).begin();
	for (; pp != d.ShowStructure(n).end(); pp++){
		if (s == (*pp).nameoffile){
			return False;
		}
	}
	if (s == "..."){
		
		return False;
	}
	return True;
}

void PrintAccessTable(std::vector<access> m){
	std::cout.width(10);
	std::cout << "Name" << std::setw(10) << "ID" << std::setw(5)<<"Read" << std::setw(5)<<"Write" << endl;
	for (size_t i = 0; i < m.size(); i++){
		std::cout << (i + 1) << ". " << std::setw(7) << m[i].UserName.name << std::setw(10) << m[i].UserName.ID << std::setw(5)
			<< m[i].read << std::setw(5) << m[i].write << std::endl;
	}
}

set<structelem>::iterator ChooseFile(filesystem &s){
	ShowDirectory(*s.GetNowDirectory(), s.GetNowUser());
	cout << "Choose file." << endl;
	_flushall();
	char j[200];
	cin.getline(j, 199);
	string xz(j);
	if (s.GetNowDirectory()->getparent() && xz == "..."){
		
		throw check_parent;
	}
	set<structelem>::iterator pp = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
	for (; pp != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp++){
		if (xz == (*pp).nameoffile){
			break;
		}
	}
	return pp;
}