// Test.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"


TEST(FilesystemMethods, BeginWork){
	filesystem s;
	s.BeginWork(s.getTable()[0]);
	user u = s.getTable()[0];
	EXPECT_EQ(u.ID, s.GetNowUser().ID);
	EXPECT_EQ(u.key, s.GetNowUser().key);
	EXPECT_EQ(u.name, s.GetNowUser().name);
	EXPECT_EQ(&(s.SetRoot()), s.GetNowDirectory());
}

TEST(FilesystemMethods, EncryptionFile1){
	filesystem s;
	s.BeginWork(s.getTable()[0]);
	std::vector<struct access> a;
	
	struct access d = { s.GetNowUser(), True, True, True };
	a.push_back(d);
	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);
	simplefile f(a, "1",s.GetNowDirectory(),s.GetNowUser(), 0, nulld);	
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	s.EncryptionFile(f);
	EXPECT_EQ("Encrypted File", s.SetRoot().setstructtable().begin()->descriptoroffile->iAm());
	
	encryptedfile *ef=dynamic_cast<encryptedfile *>(s.SetRoot().setstructtable().begin()->descriptoroffile);
	s.EncryptionFile(*ef);
	EXPECT_EQ("Simple File", s.SetRoot().setstructtable().begin()->descriptoroffile->iAm());
	simplefile *sf = dynamic_cast<simplefile *>(s.SetRoot().setstructtable().begin()->descriptoroffile);
	EXPECT_EQ("privet", sf->getstream()[0].data);
}

TEST(FilesystemMethods, Statistic){
	filesystem s;
	s.BeginWork(s.getTable()[0]);

	std::vector<struct access> a;
	struct access d = { s.GetNowUser(), True, True, True };
	a.push_back(d);

	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);

	simplefile f(a, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	descriptor m = {"SYMKEY", 0, "5"};
	nulld.push_back(m);
	encryptedfile f2(a, "2", s.GetNowDirectory(), s.GetNowUser(), 10, nulld, s.getTable());
	structelem t1 = { f2.getname(), &f2 };
	s.SetRoot().setstructtable().insert(t1);

	directory gen(a, "3", s.GetNowDirectory());
	structelem t2 = { gen.getname(), &gen };
	s.SetRoot().setstructtable().insert(t2);

	statistic g = { 0, 0, 0, 0 };
	g = s.Statistic(g, s.GetNowDirectory());
	
	EXPECT_EQ(1, g.directorynumber);
	EXPECT_EQ(1, g.encryptedfilenumber);
	EXPECT_EQ(1, g.simplefilenumber);
	EXPECT_EQ(0, g.allsize);
}

TEST(SimpleFileMethod, Condition){
	filesystem s;
	s.BeginWork(s.getTable()[0]);
	user u = s.getTable()[0];
	std::vector<struct access> a1;
	struct access d = { s.GetNowUser(), True, True, True };
	a1.push_back(d);
	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);
	simplefile f(a1, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	OpenClose a;
	a = OpenRead;
	f.ConditionFile(a, u);
	EXPECT_EQ(a, f.getcondition()); 
	a = OpenWrite;
	f.ConditionFile(a, u);
	EXPECT_EQ(a, f.getcondition());
	a = OpenEdit;
	f.ConditionFile(a, u);
	EXPECT_EQ(a, f.getcondition());
	a = Close;
	f.ConditionFile(a, u);
	EXPECT_EQ(a, f.getcondition());
}

TEST(SimpleFileMethod, CopyFile){
	filesystem s;
	s.BeginWork(s.getTable()[0]);
	user u = s.getTable()[0];
	std::vector<struct access> a1;
	struct access d = { s.GetNowUser(), True, True, True };
	a1.push_back(d);
	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);
	simplefile f(a1, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	f.CopyFile(u, *s.GetNowDirectory());
	set<structelem>::iterator pp = s.GetNowDirectory()->setstructtable().begin();
	pp++;
	OpenClose a;
	a = OpenRead;
	
	simplefile * tdm = dynamic_cast<simplefile*>(pp->descriptoroffile);
	tdm->ConditionFile(a, u);

	EXPECT_EQ("1-Copy", (*pp).descriptoroffile->getname());
	
	EXPECT_EQ(1, (*pp).descriptoroffile->getaccesssize());
	
	EXPECT_EQ(1000, (*pp).descriptoroffile->setaccess()[0].UserName.ID);
	
	EXPECT_EQ("privet\n", tdm->ShowInformation());
}

TEST(SimpleFileMethod, MovingFile){
	filesystem s;
	s.BeginWork(s.getTable()[0]);

	std::vector<struct access> a;
	struct access d = { s.GetNowUser(), True, True, True };
	a.push_back(d);

	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);

	simplefile f(a, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	directory gen(a, "3", s.GetNowDirectory());
	structelem t2 = { gen.getname(), &gen };
	s.SetRoot().setstructtable().insert(t2);

	f.MovingFile(s.GetNowUser(), gen);

	set<structelem>::iterator pp = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
	for (; pp != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp++){
		if ("1" == (*pp).nameoffile){
			break;
		}
	}
	EXPECT_EQ(s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(), pp);

	s.SetNowDirectory(&gen);
	set<structelem>::iterator pp1 = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
	for (; pp1 != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp1++){
		if ("1" == (*pp1).nameoffile){
			break;
		}
	}
	EXPECT_EQ(s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin(), pp1);
}

TEST(SimpleFileMethod, DeleteFile){
	filesystem s;
	s.BeginWork(s.getTable()[0]);

	std::vector<struct access> a;
	struct access d = { s.GetNowUser(), True, True, True };
	a.push_back(d);

	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);

	simplefile f(a, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	f.DeleteFile(s.GetNowUser());

	set<structelem>::iterator pp = s.GetNowDirectory()->ShowStructure(s.GetNowUser()).begin();
	for (; pp != s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(); pp++){
		if ("1" == (*pp).nameoffile){
			break;
		}
	}
	EXPECT_EQ(s.GetNowDirectory()->ShowStructure(s.GetNowUser()).end(), pp);
}

TEST(SimpleFileMethod, RenameFile){
	filesystem s;
	s.BeginWork(s.getTable()[0]);

	std::vector<struct access> a;
	struct access d = { s.GetNowUser(), True, True, True };
	a.push_back(d);

	vector<descriptor> nulld;
	descriptor de = { "MAIN", 0, "privet" };
	nulld.push_back(de);

	simplefile f(a, "1", s.GetNowDirectory(), s.GetNowUser(), 0, nulld);
	structelem t = { f.getname(), &f };
	s.SetRoot().setstructtable().insert(t);

	f.RenameFile(s.GetNowUser(), "New_Name");

	EXPECT_EQ("New_Name", s.SetRoot().setstructtable().begin()->nameoffile);

}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
