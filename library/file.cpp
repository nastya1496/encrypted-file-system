#include "stdafx.h"
#include "file.h"
#include "iomanip"
using namespace std;

std::vector<descriptor> nulldesc;
std::vector<user> nulluser;
std::vector<access> nullvector;



file::~file()
{
}
directory::~directory()
{
}


ostream& operator << (ostream& os, const struct time& t){
	os << t.hour << ":" << t.minute << ":" << t.second << "  " << t.day << "." << t.month << "." << t.year;
	return os;
}

ostream& operator << (ostream& os, const descriptor& t){
	os << t.nameofstream;
	return os;
}

ostream& operator << (ostream& os, const structelem& t){
	if (t.descriptoroffile->iAm() == "Directory"){
		os << '[' << t.nameoffile << ']';
		directory *d = dynamic_cast<directory *>(t.descriptoroffile);
		os << setw(9) << d->getsize() << " bytes";
	}
	else{
		os <<t.nameoffile;
		simplefile *f = dynamic_cast<simplefile *>(t.descriptoroffile);
		os << setw(10) << f->getsize() << " bytes";
	}
	return os;
}

bool operator < (const structelem &a, const structelem &b){
	return a.nameoffile < b.nameoffile;
}

int findaccess(user n, const vector<access> &a){
	for (size_t i = 0; i < a.size(); i++){
		if (n.ID == a[i].UserName.ID){
			return i;
		}
	}
	return -1;
}









file::file() :AccessList(nullvector), nameoffile(""), parent(nullptr){}

file::file(const std::vector<access> &AccessList = nullvector, const string & nameoffile = " ", directory *parent = nullptr)
	: AccessList(AccessList), nameoffile(nameoffile), parent(parent){}


const vector<access> & file::getaccess() const{
	return AccessList;
}

int file::getaccesssize() const{
	return AccessList.size();
}

string& file::getname() {
	return nameoffile;
}

void file::setname(const string& a){
	nameoffile.clear();
	nameoffile += a;
}

void file::changeparent(directory *a){
	parent = a;
}

directory* file::getparent(){
	return parent;
}

vector<access> & file::setaccess(){
	return AccessList;
}











simplefile::simplefile() :file(nullvector, ""), condition(Close), data(createtime()), owner({ -1 }), sizesimplefile(0), streamtable(nulldesc){}

simplefile::simplefile(const std::vector<access> &AccessList, const string & nameoffile = " ", directory *parent = nullptr,
	const user &owner = { -1 }, const int number = 0, const std::vector<descriptor> &stream = nulldesc)
	: file(AccessList, nameoffile, parent), condition(Close), data(createtime()), owner(owner),
	sizesimplefile(0), streamtable(stream){}

simplefile::simplefile(const simplefile& a) :file(a),condition(a.condition), data(a.data), owner(a.owner), sizesimplefile(a.sizesimplefile){
	if (a.streamtable.size()){
		streamtable = a.streamtable;
	}
	else {
		streamtable = nulldesc;
	}
}

const std::vector<descriptor> & simplefile::getstream() const{
	return streamtable;
}

std::vector<descriptor> & simplefile::setstream(){
	return streamtable;
}

ostream& simplefile :: Print(ostream& os){
	os << "Condition: " << condition << std::endl
		<< "Creation time: " << data << std::endl
		<< "Creator ID: " << owner.ID << std::endl
		<< "Size: " << sizesimplefile << " bytes"<< std::endl
		<< "Stream's table: ";
	for (size_t i = 0; i < streamtable.size(); i++){
		os << streamtable[i] << std::endl;
	}
	return os;
}

const struct time simplefile::createtime() const{
	time_t t = time(nullptr);
	tm* atm = new tm;
	localtime_s(atm, &t);
	struct time tm = { atm->tm_hour, atm->tm_min, atm->tm_sec, atm->tm_year + 1900, atm->tm_mday, atm->tm_mon+1 };
	delete atm;
	return tm;
}

void simplefile::correctsize(directory *d, int r){
	if (d){
		d->setsize(d->getsize() + r);
		correctsize(d->getparent(), r);
	}
}

void simplefile::ConditionFile(OpenClose &a,const user &u){
	const vector<access> &d = getaccess();
	int n = findaccess(u, d);
	
	if (condition == a){
		throw std::exception("Operation has already.");
	}

	if (n >= 0){
		if ((a == OpenRead) && (d[n].read == True)){
			condition = a;
		}
		if ((a == OpenWrite) && (d[n].write == True)){
			condition = a;
			for (size_t i = 0; i < streamtable.size(); i++){
				streamtable[i].data.clear();
			}
		}
		if ((a == OpenEdit) && (d[n].write == True)){
			condition = a;
		}
		if (a == Close){
			condition = a;
			correctsize(getparent(), streamtable[0].data.length() - sizesimplefile);
			sizesimplefile = streamtable[0].data.length();
		}
	}
	else
	{
		
		throw std::exception("You haven't access to this file.");
	}
}

const string simplefile::ShowInformation() const{
	if (condition != Close){
		string s;
		for (size_t i = 0; i < streamtable.size(); i++){
			s += streamtable[i].data;
			s += '\n';
		}
		return s;
	}
	else {
		throw std::exception("File isn't open.");
	}
}

string& simplefile::EditInformation(){
	if ((condition == OpenEdit)||(condition==OpenWrite)){
		return streamtable[0].data;
	}
	else {
		throw std::exception("file can not be change.");
	}
}

vector<access> & simplefile::EditAccessTable(const user &o){
	if (o.ID == owner.ID){
		return setaccess();
	}
	throw exception("You cannot edit AccessTable");
}

void simplefile::CopyFile(const user&u, directory &a)const{
	vector<access> d = a.getaccess();
	
	int n = findaccess(u, d);

	vector<access> e = getaccess();
	int g = findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g >= 0) && (e[g].execute == True)){
		file* f = clone();
		f->setname(f->getname()+"-Copy");
		f->changeparent(&a);
		structelem t = { f->getname(), f };
		a.setstructtable().insert(t);
		simplefile *sf=dynamic_cast<simplefile *> (f);
		sf->correctsize(sf->getparent(),sf->getsize());
	}
	else{
		throw std::exception("You has not access to this file.");
	}
	
	
}

void simplefile::MovingFile(const user&u, directory &a){
	vector<access> d = a.getaccess();
	int n = findaccess(u, d);

	vector<access> e = getaccess();
	int g = findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g >= 0) && (e[g].execute)){
		
		structelem t = { this->getname(), this };
		a.setstructtable().insert(t);
		std::set<structelem>::iterator pp = getparent()->setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != getparent()->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != getparent()->setstructtable().end()){
			getparent()->setstructtable().erase(pp);
		}
		correctsize(getparent(), -getsize());
		changeparent(&a);
		correctsize(getparent(), getsize());
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

void simplefile::DeleteFile(const user&u){
	vector<access> d = getaccess();
	int n = findaccess(u, d);

	if ((n >= 0) && (d[n].execute == True)){
		directory *p = getparent();
		correctsize(p, -getsize());
		p->setstructtable();
		std::set<structelem>::iterator pp = p->setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != p->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != p->setstructtable().end()){
			p->setstructtable().erase(pp);
		}
		AccessList.clear();
		getname().clear();
		changeparent(nullptr);
		for (size_t i = 0; i < streamtable.size(); i++){
			streamtable[i].nameofstream.clear();
			streamtable[i].data.clear();
		}
		streamtable.clear();
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

void simplefile::RenameFile(const user&u, const string&s){
	vector<access> d = getaccess();
	int n = findaccess(u, d);

	if ((n >= 0) && (d[n].execute == True)){
		setname(s);
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

user simplefile::getowner(){
	return owner;
}

int simplefile::getsize(){
	return sizesimplefile;
}







void directory::correctsize(directory *d, int r){
	if (d){
		d->setsize(d->getsize() + r);
		correctsize(d->getparent(), r);
	}
}

directory::directory() :file(nullvector), sizedirectory(0), VirtualAddress(0){}

directory::directory(const std::vector<access> &AccessList = nullvector, const string & nameoffile = " ", directory *parent,
	const int &sizedirectory, const int &VirtualAddress) :file(AccessList, nameoffile, parent),
	sizedirectory(sizedirectory), VirtualAddress(VirtualAddress){}

const std::set<structelem> & directory::ShowStructure(const user &u) const{
	vector<access> d = getaccess();
	int n = findaccess(u, d);
	if (n >= 0){
		if ((d[n].read == True) || (d[n].write == True) || (d[n].execute == True)){
			return StructTable;
		}
		else {
			throw std::exception("You have not access.");
		}
	}
	else {
		throw std::exception("You have not access.");
	}
	
}

void directory::CopyFile(const user&u,directory &a)const{
	vector<access> d = a.getaccess();
	int n = findaccess(u, d);
	

	vector<access> e = getaccess();
	int g= findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g >= 0) && (e[g].execute)){
		file* f = clone();
		
		f->setname(f->getname()+"Copy");
		f->changeparent(&a);
		structelem t = { f->getname(), f };
		a.StructTable.insert(t);
		directory *sf = dynamic_cast<directory *> (f);
		sf->correctsize(sf->getparent(), sf->getsize());
	}
	else{
		throw std::exception("You has not access to this directory.");
	}
}

std::set<structelem>& directory::setstructtable(){
	return StructTable;
}

void directory::MovingFile(const user&u, directory &a){
	vector<access> d = a.getaccess();
	int n = findaccess(u, d);

	vector<access> e = getaccess();
	int g = findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g>=0)&&(e[g].execute)){
		
		structelem t = { this->getname(), this };
		a.StructTable.insert(t);
		directory *p = getparent();
		p->setstructtable();
		std::set<structelem>::iterator pp = p->setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != p->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != p->setstructtable().end()){
			p->setstructtable().erase(pp);
		}
		correctsize(getparent(), -getsize());
		changeparent(&a);
		correctsize(getparent(), getsize());
	}
	else{
		throw std::exception("You has not access to this directory.");
	}
}

void directory::DeleteFile(const user&u){
	vector<access> d = getaccess();
	int n = findaccess(u, d);


	if ((n >= 0) && (d[n].execute == True)){
		directory *p = getparent();
		correctsize(p, -getsize());
		p->setstructtable();
		std::set<structelem>::iterator pp = p->setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != p->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != p->setstructtable().end()){
			p->setstructtable().erase(pp);
		}

		for (pp = StructTable.begin(); pp != StructTable.end(); pp++){
			(*pp).descriptoroffile->DeleteFile(u);
		}
		AccessList.clear();
		getname().clear();
		changeparent(nullptr);


	}
	else{
		throw std::exception("You has not access to this directory.");
	}
}

void directory::RenameFile(const user&u, const string&s){
	vector<access> d = getaccess();
	int n = findaccess(u, d);

	if ((n >= 0) && (d[n].execute == True)){
		setname(s);
	}
	else{
		throw std::exception("You has not access to this directory.");
	}
}

ostream& directory::Print(ostream& os){
	os << "Size: " << sizedirectory << " bytes" << std::endl
		<< "Struct's table: ";
	
	std::set<structelem>::iterator pp = StructTable.begin();//pp -�������� ��������������� ������� �������� � ����
	for (; pp != StructTable.end(); pp++){
		os << (*pp).nameoffile<<endl;
	}
	
	return os;
}




encryptedfile::encryptedfile() :simplefile(nullvector), accesstable(nulluser){}

encryptedfile::encryptedfile(const std::vector<access> &AccessList = nullvector, const string & nameoffile = " ", directory *parent = nullptr,
	const user &owner = { -1 }, const int number = 0, const std::vector<descriptor> &stream = nulldesc,
	const std::vector<user> &accesstable = nulluser) : simplefile(AccessList, nameoffile,
	parent, owner, number, stream), accesstable(accesstable){}

encryptedfile::encryptedfile(const encryptedfile& a) :simplefile(a){
	if (accesstable.size()){
		accesstable = a.accesstable;
	}
	else {
		accesstable = nulluser;
	}
}

void encryptedfile::CopyFile(const user&u, directory &a)const{
	vector<access> d = a.getaccess();
	int n = findaccess(u, d);
	
	vector<access> e = getaccess();
	int g = findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g >= 0) && (e[g].execute == True)){
		file* f = clone();
		f->setname(f->getname() + "Copy");
		f->changeparent(&a);
		structelem t = { f->getname(), f };
		a.setstructtable().insert(t);
		encryptedfile *sf = dynamic_cast<encryptedfile *> (f);
		sf->correctsize(sf->getparent(), sf->getsize());
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

void encryptedfile::MovingFile(const user&u, directory &a){
	vector<access> d = a.getaccess();
	int n = findaccess(u, d);

	vector<access> e = getaccess();
	int g = findaccess(u, e);

	if ((n >= 0) && (d[n].write == True) && (g >= 0) && (e[g].execute)){
	
		structelem t = { this->getname(), this };
		a.setstructtable().insert(t);
		std::set<structelem>::iterator pp = a.setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != getparent()->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != getparent()->setstructtable().end()){
			getparent()->setstructtable().erase(pp);
		}
		correctsize(getparent(), -getsize());
		changeparent(&a);
		correctsize(getparent(), getsize());
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

void encryptedfile::DeleteFile(const user&u){
	vector<access> d = getaccess();
	int n = findaccess(u, d);

	if ((n >= 0) && (d[n].execute == True)){
		directory *p = getparent();
		correctsize(p, -getsize());
		p->setstructtable();
		std::set<structelem>::iterator pp = p->setstructtable().begin();//pp -�������� ��������������� ������� �������� � ����
		for (; pp != p->setstructtable().end(); pp++){
			if ((*pp).descriptoroffile == this)
				break;
		}
		if (pp != p->setstructtable().end()){
			p->setstructtable().erase(pp);
		}
		AccessList.clear();
		getname().clear();
		changeparent(nullptr);
		for (size_t i = 0; i < streamtable.size(); i++){
			streamtable[i].nameofstream.clear();
			streamtable[i].data.clear();
		}
		streamtable.clear();
		accesstable.clear();

	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

void encryptedfile::RenameFile(const user&u, const string&s){
	vector<access> d = getaccess();
	int n = findaccess(u, d);

	if ((n >= 0) && (d[n].execute == True)){
		setname(s);
	}
	else{
		throw std::exception("You has not access to this file.");
	}
}

const vector<user> & encryptedfile::getaccesstable() const{
	return accesstable;
}

void encryptedfile::setaccesstable(vector<user> a){
	if (accesstable.size()){
		accesstable.clear();
	}
	accesstable = a;
}

ostream& encryptedfile::Print(ostream& os){
	simplefile f = dynamic_cast<simplefile &>(*this);
	f.Print(os);
	/*os << "Condition: " << condition << std::endl
		<< "Creation time: " << data << std::endl
		<< "Creator ID: " << owner.ID << std::endl
		<< "Size: " << sizesimplefile << std::endl
		<< "Stream's table: ";
	for (size_t i = 0; i < streamnumber; i++){
		os << streamtable[i] << std::endl;
	}*/
	for (size_t i = 0; i < accesstable.size(); i++){
		os << accesstable[i].ID << std::endl;
	}
	return os;
}
