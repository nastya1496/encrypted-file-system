#ifndef _FILE_H
#define _FILE_H

#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
#include <set>
#include <vector>
#pragma once


using namespace std;

/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� ������. �������� ��� ������, ��� ����������� ����� � ������.
*/
struct descriptor{
	string nameofstream;
	int virtualadress;
	string data;
};

///����� ��������� ��������� �����
enum OpenClose { 
	OpenRead,///< ���� ������ �� ������  
	OpenWrite,///< ���� ������ �� ������
	OpenEdit,///< ���� ������ �� ��������� 
	Close///< ���� ������
}; //edit-��������


///��������������� ������������� ���
enum OnOff { 
	False,///< ������� 
	True///< ����������
};

/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� ������������. �������� ��� ������������, ��� ������������� � ���� ����������
*/
struct user{
	int ID;
	string name;
	int key;
};


/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� ���������. ������������ ��� �������� �����. �������� ���, �����, ����, ���, ������ �������� �������� �����.
*/
struct time{
	int hour,
		minute,
		second,
		year,
		day,
		month;
};


/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� �������� ������� �������������, ������� ������ � ������.  �������� ��������� ������������, ��� ������ � ������, ������ � ��������� �����(��� ����������� ����� � ������ ����������.
*/
struct access{
	user UserName;
	OnOff read;
	OnOff write;
	OnOff execute;		//��������� ����- ���������� ������
};



/*!
������������� �������� ������ � ����� ������ � ������� �������� �����.
\param os ���������� ������. 
\param t ��������� ���������, ������ �� ������� ���������� ������� � �����.
\return �����.
*/
ostream& operator << (ostream& os, const struct time& t);


/*!
������������� �������� ��� ������ � ����� ��������� ������
\param os ���������� ������.
\param t ��������� ������, ������ �� �������� ���������� ������� � �����.
\return �����.
*/
ostream& operator << (ostream& os, const descriptor& t);




class directory;


/*!
\brief ����������� ����� File
\author ������� ���������
\version 1.0
\date ������� 2015 ����


���������� �������� ��������� �����(���, ������� ���������� � ������� �������) � �������� �������� � ���. 
*/
class file
{
private:
	string nameoffile;
	directory *parent;
protected:
	std::vector<access> AccessList;
public:
	/*!
	������ ���������������� �����������.
	*/
	file();

	/*!
	���������������� �����������
	\param AccessList ������� ������������� ������� ������ � �����.
	\param nameoffile ��� �����.
	\param parent ������������ �������.
	*/
	file(const std::vector<access> &AccessList, const string & nameoffile, directory *parent);

	/*!
	����������.
	*/
	virtual ~file();

	/*!
	����������� ������� ��� ������ ���������� � �����.
	\param os ���������� ������.
	\return �����.
	*/
	virtual ostream& Print(ostream&os)=0;

	/*!
	�������� ������� �������.
	\return ����������� ������ �� ������� ������� �����.
	*/
	const std::vector<access> & getaccess() const;

	/*!
	�������� ������ ������� �������.
	\return ������.
	*/
	int getaccesssize() const;

	/*!
	�������� ��� �����.
	\return ����������� ������ �� ��� �����.
	*/
	string & getname();

	/*!
	�������� ��� �����
	\param a ������- ����� ��� �����
	*/
	void setname(const string& a);

	/*!
	�������� ��������� �� ������������ �������.
	\return ��������� �� ������������ �������.
	*/
	directory* getparent();

	/*!
	�������� ������������ �������
	\param a ������ �� ������������ �������
	*/
	void changeparent(directory *a);

	/*!
	��������� �������� ������� �������.
	\return ������ �� ������� ������� �����.
	*/
	std::vector<access>& setaccess();

	/*!
	����������� �������. ��������� ���������� ��� �����
	\return ��� �����
	*/
	virtual const string iAm() = 0;

	/*!
	����������� �������. ������� ����� ���������� ������
	\return ��������� �� ��������� ������
	*/
	virtual file * clone() const = 0;

	/*!
	����������� �������. ������� ����� ���������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������� ����
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void CopyFile(const user& u, directory &a)const =0;

	/*!
	����������� �������. ���������� ��������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void MovingFile(const user&u, directory &a)=0;

	/*!
	����������� �������. ������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	*/
	virtual void DeleteFile(const user&u) = 0;

	/*!
	����������� �������. ���������������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param s ����� ��� ���������� ������
	*/
	virtual void RenameFile(const user&u, const string&s) = 0;
	
};



/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� �������� ����������� ������� ��������. �������� ��� �����, ������� ���������� � �����, � ��������� �� ������ ����.
*/
struct structelem{
	string& nameoffile;
	file* descriptoroffile;
};

/*!
������������� �������� ��� ������ � ����� ��������� �������� ����������� ������� ��������
\param os ���������� ������.
\param t ��������� �������� ����������� �������, ������ �� �������� ���������� ������� � �����.
\return �����.
*/
ostream& operator << (ostream& os, const structelem& t);

/*!
������������� �������� ���������
\param a, b - ��������� ��������� ����������� �������, ������� ���������� ��������
\return ��������� ��������� (���� �<b, �� True)
*/
bool operator < (const structelem &a, const structelem &b);


/*!
\brief ����� Directory
\author ������� ���������
\version 1.0
\date ������� 2015 ����


���������� ��������� ��������(������, ����������� ����� � ����������� �������) � �������� � ���.
*/
class directory :
	public file
{
private:
	int sizedirectory;  //������ �������� � ������
	int VirtualAddress;
	std::set<structelem> StructTable;

protected:

	/*!
	����������� �������. �������� ������ ������������� ��������. ������������ ��� �����������/��������/����������� ������
	\param u ��������� �� �������, ������ �������� �� ����� ��������
	\param r ���������� ����, �� ������� ������ ���������� ������
	*/
	void correctsize(directory *d, int r);

public:

	/*!
	������ ���������������� �����������.
	*/
	directory();

	/*!
	���������������� �����������
	\param AccessList ������� ������������� ������� ������ � �����.
	\param nameoffile ��� �����.
	\param parent ������������ �������.
	\param sizedirectory ������ ��������(0)
	\param VirtualAddress ����������� ������ ��������
	*/
	directory(const std::vector<struct access> &AccessList, const string & nameoffile, directory *parent = nullptr,
		const int &sizedirectory = 0, const int &VirtualAddress = 0);

	/*!
	����������.
	*/
	virtual ~directory();

	/*!
	�������� ������ ��������.
	\return ������ ��������.
	*/
	int getsize() const{ return sizedirectory; }

	/*!
	�������� ������ ��������.
	\param s ����� ������ ��������.
	*/
	void setsize(const int s){ sizedirectory = s; }

	/*!
	����������� ������� ��� ������ ���������� � ��������.
	\param os ���������� ������.
	\return �����.
	*/
	virtual ostream& Print(ostream&);

	/*!
	����������� �������. ��������� ���������� ��� �����
	\return ��� �����
	*/
	virtual const string iAm(){ string s = "Directory"; return s; }

	/*!
	�������� ����������� ������� ��������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\return ����������� ������ �� ����������� ������� ��������
	*/
	const std::set<structelem> & ShowStructure(const user &u) const;

	/*!
	����������� �������. ������� ����� ���������� ������
	\return ��������� �� ��������� ������
	*/
	virtual file * clone() const{
		return new directory(*this);
	}

	/*!
	����������� �������. ������� ����� ���������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������� ����
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void CopyFile(const user&u,directory &a)const;

	/*!
	��������� �������� ����������� ������� ��������.
	\return ������ �� ����������� ������� ��������.
	*/
	std::set<structelem> & setstructtable();

	/*!
	����������� �������. ���������� ��������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void MovingFile(const user&u, directory &a);

	/*!
	����������� �������. ������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	*/
	virtual void DeleteFile(const user&u);

	/*!
	����������� �������. ���������������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param s ����� ��� ���������� ������
	*/
	virtual void RenameFile(const user&u, const string&s);	
};


/*!
\brief ����� SimpleFile
\author ������� ���������
\version 1.0
\date ������� 2015 ����


���������� ��������� �������� �����(���������, ����� ��������, ��������� ���������, ������ � ������� ������� ��������������� � ������) � �������� � ���.
*/
class simplefile :
	public file
{
private:
	OpenClose condition;
	struct time data;
	user owner;
	int sizesimplefile;
	
protected:
	std::vector<descriptor> streamtable;

	/*!
	����������� �������. �������� ������ ������������� ��������. ������������ ��� �����������/��������/����������� ������
	\param u ��������� �� �������, ������ �������� �� ����� ��������
	\param r ���������� ����, �� ������� ������ ���������� ������
	*/
	void correctsize(directory *, int);

public:

	/*!
	��������������� ������� ��� ��������� ������� �������� �����.
	\return ��������� ��������� � ������� � ������� �������� �����.
	*/
	const struct time createtime() const;

	/*!
	������ ���������������� �����������.
	*/
	simplefile();

	/*!
	���������������� �����������
	\param AccessList ������� ������������� ������� ������ � �����.
	\param nameoffile ��� �����.
	\param parent ������������ �������.
	\param owner ��������� ������������ -��������� �����
	\param number ������ �����
	\param stream ������� �������, ��������������� � ������
	*/
	simplefile(const std::vector<struct access> &AccessList, const string & nameoffile, directory *parent,
		const user &owner, const int number, const std::vector<descriptor> &stream);

	/*!
	����������.
	*/
	virtual ~simplefile(){
		streamtable.clear();
	}

	/*!
	��������� ����� � ������� �������, ��������������� � ������.
	\param n ��������� ������, ������� ���������� �������� � �������.
	*/
	void addstream(const descriptor &n){
		streamtable.push_back(n);
	}

	/*!
	������� ����� �� ������� �������, ��������������� � ������.
	\param n ��������� ������, ������� ���������� �������.
	*/
	void delstream(const string &name){
		std::vector<descriptor>::iterator pp = streamtable.begin();
		for (; pp != streamtable.end(); pp++){
			if ((*pp).nameofstream == name){
				streamtable.erase(pp);
				break;
			}
		}
	}

	/*!
	�������� ��������� ������������, ������� ������ ����
	\return ��������� ��������� �����.
	*/
	user getowner();

	/*!
	�������� ������ �����
	\return ������ �����.
	*/
	int getsize();

	/*!
	�������� c�������� �����
	\return ��������� �����.
	*/
	OpenClose getcondition(){ return condition; }

	/*!
	�������� ������� ������� ��������������� � ������
	\return ����������� ������ �� ������� �������.
	*/
	const std::vector<descriptor> & getstream() const;

	/*!
	��������� �������� ������� ������� ��������������� � ������
	\return ������ �� ������� �������.
	*/
	std::vector<descriptor> & simplefile::setstream();

	/*!
	�������� ��������� �����.
	\param a ����� ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������� ����
	*/
	void ConditionFile(OpenClose &a,const user &u);	//��������� ��������� �����/�������� �����

	/*!
	����������� ������� ��� ������ ���������� � �����.
	\param os ���������� ������.
	\return �����.
	*/
	virtual ostream& Print(ostream&);

	/*!
	������� ����������, ������� ���������� � �����.
	\return �������� ����������.
	*/
	const string ShowInformation() const;

	/*!
	��������� �������� ����������,������������ �����
	\return ������ �� ����������, ������������ � �����
	*/
	string& EditInformation();

	/*!
	����������� �������. ��������� ���������� ��� �����
	\return ��� �����
	*/
	virtual const string iAm(){ 
		string s = "Simple File"; return s; }

	/*!
	��������� �������� ������� ������� �����
	\param � ��������� ������������, �� ����� �������� �� ����� ����������� ����
	\return ������ �� ������� �������.
	*/
	std::vector<access> & EditAccessTable(const user &o);

	/*!
	����������� �������. ������� ����� ���������� ������
	\return ��������� �� ��������� ������
	*/
	virtual file * clone() const{
		return new simplefile(*this);
	}

	/*!
	���������� �����������
	\param ������ �� ��������� ������
	*/
	simplefile(const simplefile& a);

	/*!
	����������� �������. ������� ����� ���������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������� ����
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void CopyFile(const user&u, directory &a)const;

	/*!
	����������� �������. ���������� ��������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void MovingFile(const user&u, directory &a);

	/*!
	����������� �������. ������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	*/
	virtual void DeleteFile(const user&u);

	/*!
	����������� �������. ���������������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param s ����� ��� ���������� ������
	*/
	virtual void RenameFile(const user&u, const string&s);
};


/*!
\brief ����� EncryptedFile
\author ������� ���������
\version 1.0
\date ������� 2015 ����


���������� ��������� �������������� �����(������� �������������, ������� ������ ������������ �����) � �������� � ���.
*/
class encryptedfile :
	public simplefile
{
private:
	std::vector<user> accesstable;	
public:

	/*!
	������ ���������������� �����������.
	*/
	encryptedfile();

	/*!
	�������� ������� ������� �����
	\param a ����� �������  �������
	*/
	void setaccesstable(std::vector<user> a);

	/*!
	�������� ������� ������� �����
	\return ������ �� ������� �������.
	*/
	const std::vector<user> & getaccesstable() const;

	/*!
	���������������� �����������
	\param AccessList ������� ������������� ������� ������ � �����.
	\param nameoffile ��� �����.
	\param parent ������������ �������.
	\param owner ��������� ������������ -��������� �����
	\param number ������ �����
	\param stream ������� �������, ��������������� � ������
	\param accesstable ������� �������������, ������� ������ � �����������
	*/
	encryptedfile(const std::vector<struct access> &AccessList, const string & nameoffile, directory *parent,
		const user &owner, const int number, const std::vector<descriptor> &stream,
		const std::vector<user> &accesstable);

	/*!
	����������.
	*/
	virtual ~encryptedfile(){
		accesstable.clear();
	}

	/*!
	����������� ������� ��� ������ ���������� � �����.
	\param os ���������� ������.
	\return �����.
	*/
	virtual ostream& Print(ostream&);

	/*!
	����������� �������. ��������� ���������� ��� �����
	\return ��� �����
	*/
	virtual const string iAm(){ string s = "Encrypted File"; return s; }

	/*!
	���������� �����������
	\param ������ �� ��������� ������
	*/
	encryptedfile(const encryptedfile& a);

	/*!
	����������� �������. ������� ����� ���������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������� ����
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void CopyFile(const user&u, directory &a)const;

	/*!
	����������� �������. ���������� ��������� ������ � ��������� �����
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param a �������, � ������� ���������� ����������� ����
	*/
	virtual void MovingFile(const user&u, directory &a);

	/*!
	����������� �������. ������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	*/
	virtual void DeleteFile(const user&u);

	/*!
	����������� �������. ���������������� ��������� ������
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � ������
	\param s ����� ��� ���������� ������
	*/
	virtual void RenameFile(const user&u, const string&s);
};


/*!
��������������� ������� ��� ������ ������������ � ������� ������� �����
\param n ��������� ������������, ������� �������� �� ����� ����������������� � ������ ��� ������
\param � ������� ������� �����, � ������� �� ���������� �����������������
\return ��������� ������(0 -��� ����� ������ ��������� ������������ � �������)
*/
int findaccess(user n, const std::vector<access> &a);

#endif