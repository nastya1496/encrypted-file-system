#include "stdafx.h"
#include "filesystem.h"

const user filesystem::ADMIN = { 1000, "Admin", 1 };




filesystem::~filesystem()
{
}


void filesystem::BeginWork(const user&u){
	nowuser = u;
	nowdirectory = root;
}

std::vector<user> & filesystem::EditTable(){
	if (nowuser.ID == ADMIN.ID){
		return usertable;
	}
	else{
		throw std::exception("You hasn't access rights");
	}
}

void filesystem::EncryptionFile(simplefile &a){
	if (nowuser.ID == a.getowner().ID){
		int k = 1 + rand() % 21;
		int b;
		descriptor nd = a.getstream()[0];
		for (size_t i = 0; i < nd.data.length(); i++){
			b=nd.data[i];
			b += k;
			nd.data[i] = char(b);
			//(a.getstream())[0].data[i]=char(b);
		}
		a.delstream(nd.nameofstream);
		a.addstream(nd);
		nd = { "SYMKEY", 0, ""};
		for (int j = 0; j < a.getaccesssize(); j++){
			int t = a.getaccess()[j].UserName.key;
			t+= k;
			nd.data += to_string(t);
			nd.data += ' ';
		}
		a.addstream(nd);
		int s=a.getaccesssize();
		encryptedfile* l = new encryptedfile (*static_cast<encryptedfile*>(&a));//�������� ������������� ���� l � ���������� ��� ���� ������
		std::vector<user> g;
		for (int j = 0; j < s; j++){
			g.push_back((a.getaccess())[j].UserName);
		}
		l->setaccesstable(g);

		std::set<structelem> &d = a.getparent()->setstructtable();
		a.DeleteFile(nowuser);

		/*
		std::set<structelem>::iterator pp = d.begin();
		for (; pp !=d.end(); pp++){
			if ((*pp).descriptoroffile == &a)
				break;
		}
		d.erase(pp);*/

		structelem ll = { l->getname(), l };
		d.insert(ll);

	}
	else{
		throw std::exception("You hasn't access rights");
	}
}

void filesystem::EncryptionFile(encryptedfile &a){
	if (nowuser.ID == a.getowner().ID){
		int k;
		char cc[7];
		string ss=a.getstream()[1].data;
		for (size_t i = 0; ss[i] != ' '; i++){
			cc[i] = ss[i];
		}
		k=atoi(cc);// ����������� ������� � ����
		k--;


		int b;
		descriptor nd = a.getstream()[0];
		for (size_t i = 0; i < nd.data.length(); i++){
			b = nd.data[i];
			b -= k;
			nd.data[i] = char(b);
			//(a.getstream())[0].data[i]=char(b);
		}
		a.delstream("MAIN");
		a.delstream("SYMKEY");
		a.addstream(nd);


		simplefile *opa = new simplefile(*dynamic_cast<simplefile*>(&a));
		std::set<structelem> &d = a.getparent()->setstructtable();
		a.DeleteFile(nowuser);

		structelem ll = { opa->getname(), opa };
		d.insert(ll);

	}
}

statistic filesystem::Statistic(statistic& a,directory *p){
	
	std::set<structelem> d = p->setstructtable();
	std::set<structelem>::iterator pp = d.begin();//pp -�������� ��������������� ������� �������� � ����
	for (; pp != d.end(); pp++){
		string t=(*pp).descriptoroffile->iAm();
		if (t == "Simple File"){
			a.simplefilenumber++;
			simplefile*pr = dynamic_cast<simplefile*>((*pp).descriptoroffile); // �������������� ����
			a.allsize += pr->getsize();
			continue;
		}
		if (t == "Encrypted File"){
			a.encryptedfilenumber++;
			simplefile*pr = dynamic_cast<simplefile*>((*pp).descriptoroffile); // �������������� ����
			a.allsize += pr->getsize();
			continue;
		}
		if (t == "Directory"){
			a.directorynumber++;
			directory*pr = dynamic_cast<directory*>((*pp).descriptoroffile);
			a=Statistic(a, pr);
		}
	}
	return a;
}