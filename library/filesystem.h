#pragma once

#ifndef _FILESYS_H
#define _FILESYS_S

#include "file.h"
#include "vector"

/*!
\brief ���������
\author ������� ���������
\version 1.0
\date ������� 2015 ����


��������� ����������. �������� ���������� ���������, ������� ������,������������� ������ � ����� ������ �������� �������.
*/
struct statistic{
	int directorynumber,
		simplefilenumber,
		encryptedfilenumber,
		allsize;
};

/*!
\brief ����� filesystem
\author ������� ���������
\version 1.0
\date ������� 2015 ����


���������� ��������� �������� �������(������, ������� �������������, �������� �������������, �������� �������, ������������� � ��������� �������� �������������) � �������� � ���.
*/
class filesystem
{
private:
	directory *root;
	std::vector<user> usertable;
	user nowuser;
	directory *nowdirectory;
	static const user ADMIN;
	int lastID;

public:

	/*!
	���������������� �����������
	*/
	filesystem() {
		std::vector<access> as;
		access ac;
		ac.UserName = {1000,"Admin",1};
		ac.execute = True;
		ac.read = True;
		ac.write = True;
		as.push_back(ac);
		root = new directory(as, "Root");
		usertable.push_back(ADMIN);
		nowuser = { 0 };
		nowdirectory = nullptr;
		lastID = 1000;

	}

	/*!
	����������
	*/
	~filesystem();

	/*!
	�������� ������ � ��������. ������������� ��������� ������������ � �������.
	\param u ��������� ������������, �� ����� �������� �� ����� ����������������� � �������� ��������
	*/
	void BeginWork(const user&u);

	/*!
	��������� �������� ������� �������������
	\return ������ �� ������� �������������
	*/
	std::vector<user> & EditTable();

	/*!
	�������� ������� �������������
	\return ����������� ����� �� ������� �������������
	*/
	const std::vector<user> & getTable() const{ return usertable; }

	/*!
	������������� ������� ����, ��������� ��� � �������������
	\param a ������ �� ������� ����
	*/
	void EncryptionFile(simplefile &a);

	/*!
	����������� �������, ������ ���������� �� �������� �������
	\param a �������������� ���������
	\param d �������, ��� �������� ����������� ����������
	\return �������������� ���������
	*/
	statistic Statistic(statistic& a, directory *d);//

	/*!
	�������� ��������� ������������
	\param u ����� ������������
	*/
	void  Setnowuser(const user&u) { nowuser = u; }

	/*!
	�������� ��������� ������������
	\return ����������� ������ �� ��������� ������������
	*/
	const user &GetNowUser() const{ return nowuser; }

	/*!
	�������� �������� �������
	\return ������ �� �������� �������
	*/
	directory* GetNowDirectory() const { return nowdirectory; }

	/*!
	�������� �������� �������
	\param a ����� �������� �������
	*/
	void SetNowDirectory(directory *a){
		nowdirectory = a;
	}

	/*!
	�������� ��������� �������� �������������
	\return �������������
	*/
	int GetLastID(){
		return lastID;
	}

	/*!
	�������� ��������� ��������� ������������
	\param n ����� ��������� �������� �������������
	*/
	void SetLastID(int n){
		lastID=n;
	}

	/*!
	��������� �������� �������� �������
	\return ������ �� �������� �������
	*/
	directory& SetRoot(){
		return *root;
	}

	/*!
	�������������� ������������� ����, ��������� ��� � �������
	\param a ������ �� ������������� ����
	*/
	void EncryptionFile(encryptedfile &a);
};


#endif
